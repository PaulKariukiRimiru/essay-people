import { Link } from 'react-router-dom';

import './footer.scss';

const pages = [
  {
    title: 'How we work',
    url: '/about',
  },
  {
    title: 'Pricing',
    url: '/pricing',
  },
  {
    title: 'Contact',
    url: '/contact',
  },
  {
    title: 'FAQ',
    url: '/faq',
  },
];

const socials = [
  {
    image: 'https://res.cloudinary.com/mikekrantz/image/upload/v1569558477/Path_38.png',
    name: 'Facebook',
    url: '',
  },
  {
    image: 'https://res.cloudinary.com/mikekrantz/image/upload/v1569558477/Path_39.png',
    name: 'Twitter',
    url: '',
  },
  {
    image: 'https://res.cloudinary.com/mikekrantz/image/upload/v1569558477/Path_40.png',
    name: 'Instagram',
    url: '',
  },
];

export const Footer = () => (
  <div className="footer">
    <div className="footer-links">
      {pages.map(({ title, url }, index) => (
        <Link key={index} className="footer-links__item" to={url}>
          {title}
        </Link>
      ))}
    </div>
    <div className="footer-socials">
      {socials.map(({ name, image, url }, index) => (
        <div className="footer-socials__item" key={index}>
          <img className="footer-socials__item-image" src={image} alt="" />
          <a href={url} className="footer-socials__item-link">
            {name}
          </a>
        </div>
      ))}
    </div>
    <div className="footer-contact">
      <div className="footer-contact-title">Get in touch with Us</div>
      <div className="footer-contact__container">
        <input
          type="email"
          name=""
          className="footer-contact__container-input"
          placeholder="Email address"
        />
        <div className="footer-contact__container-button">Send</div>
      </div>
    </div>
  </div>
);
