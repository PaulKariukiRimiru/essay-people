
import { Link, useNavigate } from 'react-router-dom';

import './Header.scss';

const pages = [
  {
    title: 'How we work',
    url: '/about',
  },
  {
    title: 'Pricing',
    url: '/pricing',
  },
  {
    title: 'Contact',
    url: '/contact',
  },
  {
    title: 'FAQ',
    url: '/faq',
  },
  {
    title: 'Order Now',
    url: '/order',
  },
  {
    title: 'Dashboard',
    url: '/dashboard',
  },
];

export const Header = () => {
  const authToken = localStorage.getItem('auth_token');
  const navigate = useNavigate();

  const logoutUser = async () => {
    localStorage.removeItem('auth_token');
    navigate('/');
  };

  return (
    <div className="header">
      <Link className="header-title" to="/">
        ESSAY PEOPLE
      </Link>
      <div className="header-nav">
        {authToken ? (
          pages.map((page, index) => (
            <Link key={index} className="header-nav__item" to={page.url}>
              {page.title}
            </Link>
          ))
        ) : (
          <Link className="header-nav__item" to="/auth">
            Sign up / Sign in
          </Link>
        )}
        <div onClick={logoutUser} className="header-nav__item">
          Logout
        </div>
      </div>
    </div>
  );
};
