import { Audio } from "react-loader-spinner";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./loader.scss";

export interface ILoaderProps {
  open: boolean;
}

const MyLoader = ({ open }: ILoaderProps) => {
  return (
    <div className={`loader-container${!open ? "__invisible" : ""}`}>
      <Audio color="#00BFFF" height={100} width={100} visible={open} />
    </div>
  );
};

export default MyLoader;
