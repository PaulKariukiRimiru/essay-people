
import './Login.scss';
import { TextField } from '@material-ui/core';

export const Login = ({ state, handleChange }) => {
  const [loginState] = state;

  return (
    <div className="login">
      <TextField
        id="outlined-required"
        label="Email"
        className="login-textField"
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        name="email"
        value={loginState.email}
      />
      <TextField
        id="outlined-password-input"
        label="Password"
        className="login-textField"
        type="password"
        autoComplete="current-password"
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        name="password"
        value={loginState.password}
      />
    </div>
  );
};
