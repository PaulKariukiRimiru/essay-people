
import { TextField } from '@material-ui/core';

import './Register.scss';

export const Register = ({ state, handleChange }) => {
  const [registerState] = state;

  return (
    <div className="register">
      <TextField
        required={true}
        id="outlined-required"
        label="Username"
        className="login-textField"
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        name="username"
        value={registerState.username}
      />
      <TextField
        required={true}
        id="outlined-required"
        label="Email"
        className="login-textField"
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        name="email"
        value={registerState.email}
      />
      <TextField
        id="outlined-password-input"
        label="Password"
        className="login-textField"
        type="password"
        autoComplete="current-password"
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        name="password"
        value={registerState.password}
      />
    </div>
  );
};
