import styled from 'styled-components';

interface IStyledCardProps {
  width?: string;
  height?: string;
  radius: string;
  backgroundtype: 'image' | 'color';
  background: string;
  color: string;
  margin?: string;
  boxShadow?: string;
}
export const StyledCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  ${(props: IStyledCardProps) => props.width && "width:" +props.width};
  ${(props: IStyledCardProps) => props.height && "height:" +props.height};
  border-radius: ${(props: IStyledCardProps) => props.radius};
  background: ${(props: IStyledCardProps) => props.backgroundtype === 'image'
    ? 'url('+props.background+')'
    : props.background
  };
  margin-left: ${(props: IStyledCardProps) => props.margin || '0px'};
  margin-right: ${(props: IStyledCardProps) => props.margin || '0px'};
  box-shadow: ${(props: IStyledCardProps) => props.boxShadow || 'none'};
  color: ${(props: IStyledCardProps) => props.color}
`;