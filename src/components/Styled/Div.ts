import styled from 'styled-components';

interface IStyledDivProps {
  width: string;
  height: string;
  backgroundColor: string;
  backgroundImage?: string;
  color: string;
  margin?: string;
}
export const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  object-fit: cover;
  width: ${(props: IStyledDivProps) => props.width};
  height: ${(props: IStyledDivProps) => props.height};
  background-image: ${(props: IStyledDivProps) => 'url(' + props.backgroundImage + ')'};
  background-color: ${(props: IStyledDivProps) => props.backgroundColor};
  margin-left: ${(props: IStyledDivProps) => props.margin || '0px'};
  margin-right: ${(props: IStyledDivProps) => props.margin || '0px'};
  color: ${(props: IStyledDivProps) => props.color};
`;
