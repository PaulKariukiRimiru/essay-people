import styled from 'styled-components';

export const StepContainer = styled.div`
  border: 1px solid rgba(112, 112, 112, 0.5);
  font-size: 22px;
  color: rgba(112, 112, 112, 1);
  border-radius: 5px;
  font-style: normal;
  font-family: Lato;
`;

export const StepHeader = styled.div`
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid rgba(112, 112, 112, 0.5);
  padding: 15px 40px;
`;

export const StepDescription = styled.span`
  font-weight: bold;
  margin-bottom: 10px;
`;

export const CurrentStep = styled.span`
  font-weight: normal;
`;

export const FormContent = styled.div`
  padding: 40px;
`;

export const DescriptionFieldContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  span {
    display: block;
    align-self: flex-end;
  }
  p {
    margin-top: 30px;
  }
`;

export const InputWithLabel = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px 0px;
  span {
    display: block;
    align-self: flex-start;
  }
`;

export const InputRow = styled.div`
  display: grid;
  grid-template-columns: minmax(100px, 300px) 100px 1fr 1fr;
  grid-gap: 20px;
`;

export const BorderedContainer = styled.div`
  border: 1px solid rgba(112, 112, 112, 0.5);
  padding: 20px;
  margin: 20px;
`;

export const SecondaryHeading = styled.p`
  display: block;
  font-weight: bold;
  font-size: 16px;
  color: rgba(112, 112, 112, 1);
  border-radius: 3px;
  margin: 12px 0;
`;
