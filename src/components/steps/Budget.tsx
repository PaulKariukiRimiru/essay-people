import { Button, TableCell } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

function createData(topic: string, paper: string, price: number, quantity: number, total: number) {
  return { topic, paper, price, quantity, total };
}

const rows = [createData('The red Jumpsuit apparatus', ' Academic Paper', 6.0, 24, 4.0)];

const Budget = () => (
  <Paper>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Topic</TableCell>
          <TableCell align="right">Paper</TableCell>
          <TableCell align="right">Unit Price</TableCell>
          <TableCell align="right">Quantity </TableCell>
          <TableCell align="right">Total Price</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map(row => (
          <TableRow key={row.topic}>
            <TableCell component="th" scope="row">
              {row.topic}
            </TableCell>
            <TableCell align="right">{row.paper}</TableCell>
            <TableCell align="right">{row.price}</TableCell>
            <TableCell align="right">{row.quantity}</TableCell>
            <TableCell align="right">{row.total}</TableCell>
          </TableRow>
        ))}
        <TableRow>
          <TableCell rowSpan={2} />
          <TableCell colSpan={1}>Estimated Total</TableCell>
          <TableCell align="right">$60</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            {' '}
            <Button variant="outlined">Confirm Price</Button>
          </TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Paper>
);

export default Budget;
