
import { TextField, Typography } from '@material-ui/core';

import {
  DescriptionFieldContainer,
  FormContent,
  StepContainer,
  StepHeader,
} from '../Styled/StepContainer';
import { IStepState } from './interfaces';
import './topic.scss';

const Description = ({ state, inputFile }: IStepState & { inputFile: any }) => {
  const [formState, setState] = state;
  const { description } = formState;

  const handleOnChange = event => {
    setState({
      ...formState,
      description: {
        ...formState.description,
        [event.target.name]: event.target.value,
      },
    });
  };

  return (
    <StepContainer>
      <StepHeader>
        <Typography variant="button" component="span">
          Topic
        </Typography>
        <Typography variant="subtitle2" component="span">
          Step 1 of 6
        </Typography>
      </StepHeader>

      <FormContent>
        <Typography variant="subtitle1" component="span">
          A good description should be
        </Typography>
        <ul>
          <li>
            <Typography variant="subtitle1" component="span">
              A good description should be
            </Typography>
          </li>
          <li>
            <Typography variant="subtitle1" component="span">
              A good description should be
            </Typography>
          </li>
          <li>
            <Typography variant="subtitle1" component="span">
              A good description should be
            </Typography>
          </li>
        </ul>

        <DescriptionFieldContainer>
          <TextField
            id="outlined-textarea"
            label="Description"
            rows={8}
            placeholder="Description"
            multiline={true}
            fullWidth={true}
            margin="normal"
            variant="outlined"
            name="description"
            value={description.description}
            onChange={handleOnChange}
          />
          <Typography component="span">(minimum 50 words)</Typography>
        </DescriptionFieldContainer>

        <DescriptionFieldContainer>
          <div className="uploader__container">
            <div className="uploader__button">
              <input type="file" id="file" ref={inputFile} multiple={true} />
              <Typography component="div">Drag or upload project files </Typography>
            </div>
          </div>
          <Typography component="p">Files should not be greater than 100mb</Typography>
        </DescriptionFieldContainer>
      </FormContent>
    </StepContainer>
  );
};

export default Description;
