import DateFnsUtils from '@date-io/date-fns';
import { FormControlLabel, MenuItem, Radio, TextField, Typography } from '@material-ui/core';
import {
  KeyboardDatePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import {
  FormContent,
  InputRow,
  InputWithLabel,
  StepContainer,
  StepHeader,
} from '../Styled/StepContainer';
import { academicLevels, allPages, styles } from './fixtures';
import './topic.scss';
import { IStepState } from './interfaces';

const Details = ({ state }: IStepState) => {
  const [formState, setState] = state;
  const { details } = formState;

  const handleOnChange = event => {
    setState({
      ...formState,
      details: {
        ...formState.details,
        [event.target.name]: event.target.value,
      },
    });
  };

  const handleDateSet = field => date => {
    setState({
      ...formState,
      details: {
        ...formState.details,
        [field]: date.toString(),
      },
    });
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <StepContainer>
        <StepHeader>
          <Typography variant="button" component="span">
            Details
          </Typography>
          <Typography variant="subtitle2" component="span">
            Step 2 of 6
          </Typography>
        </StepHeader>

        <FormContent>
          <Typography variant="subtitle1" component="span">
            Fill in the Details relating to your Order
          </Typography>

          <InputRow>
            <InputWithLabel>
              <Typography component="span">Academic Level</Typography>
              <TextField
                id="outlined-select-currency"
                select={true}
                label="Select"
                name="academicLevel"
                value={details.academicLevel || 'BSC'}
                onChange={handleOnChange}
                margin="normal"
                variant="outlined"
              >
                {academicLevels.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </InputWithLabel>

            <InputWithLabel>
              <Typography component="span">No. of Pages</Typography>
              <TextField
                select={true}
                name="pages"
                value={details.pages || '1'}
                onChange={handleOnChange}
                margin="normal"
                variant="outlined"
              >
                {allPages.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </InputWithLabel>

            <InputWithLabel>
              <Typography component="span">Deadline Date</Typography>
              <KeyboardDatePicker
                margin="normal"
                id="deadline-date-picker"
                format="MM/dd/yyyy"
                value={details.deadlineDate || new Date()}
                inputVariant={'outlined'}
                onChange={handleDateSet('deadlineDate')}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </InputWithLabel>

            <InputWithLabel>
              <Typography component="span">Deadline Time</Typography>
              <KeyboardTimePicker
                margin="normal"
                id="time-picker"
                inputVariant={'outlined'}
                value={details.deadlineTime || new Date()}
                onChange={handleDateSet('deadlineTime')}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </InputWithLabel>
          </InputRow>

          <InputRow>
            <InputWithLabel>
              <Typography component="span">Discipline</Typography>
              <TextField
                name="discipline"
                value={details.discipline || ''}
                onChange={handleOnChange}
                margin="normal"
                variant="outlined"
              />
            </InputWithLabel>
            <InputWithLabel>
              <Typography component="span">Spacing</Typography>

              <div className={'row'}>
                <FormControlLabel
                  value={details.spacing || 'single'}
                  control={
                    <Radio
                      checked={details.spacing === 'single'}
                      onChange={handleOnChange}
                      value="single"
                      name="spacing"
                      color="primary"
                      inputProps={{ 'aria-label': 'single' }}
                    />
                  }
                  label="Single"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value={details.spacing}
                  control={
                    <Radio
                      checked={details.spacing === 'double'}
                      onChange={handleOnChange}
                      value="double"
                      name="spacing"
                      color="primary"
                      inputProps={{ 'aria-label': 'Double' }}
                    />
                  }
                  label="Double"
                  labelPlacement="end"
                />
              </div>
            </InputWithLabel>
          </InputRow>

          <InputRow>
            <InputWithLabel>
              <Typography component="span">Style</Typography>
              <TextField
                select={true}
                name="style"
                value={details.style || 'APA'}
                onChange={handleOnChange}
                margin="normal"
                variant="outlined"
              >
                {styles.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </InputWithLabel>

            <InputWithLabel>
              <Typography component="span">Bibliography</Typography>

              <div className={'row'}>
                <FormControlLabel
                  value={details.bibliography}
                  control={
                    <Radio
                      checked={details.bibliography === 'Yes'}
                      onChange={handleOnChange}
                      value={'Yes'}
                      name="bibliography"
                      color="primary"
                      inputProps={{ 'aria-label': 'bibliography' }}
                    />
                  }
                  label="Yes"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value={details.bibliography}
                  control={
                    <Radio
                      checked={details.bibliography === 'No'}
                      onChange={handleOnChange}
                      value={'No'}
                      color="primary"
                      name="bibliography"
                      inputProps={{ 'aria-label': 'bibliography' }}
                    />
                  }
                  label="No"
                  labelPlacement="end"
                />
              </div>
            </InputWithLabel>
          </InputRow>

          <InputRow>
            <InputWithLabel>
              <Typography component="span">Words</Typography>
              <TextField
                name="words"
                value={details.words || ''}
                onChange={handleOnChange}
                margin="normal"
                variant="outlined"
              />
            </InputWithLabel>
          </InputRow>
        </FormContent>
      </StepContainer>
    </MuiPickersUtilsProvider>
  );
};

export default Details;
