
import { Grid, Typography } from '@material-ui/core';
import { differenceInDays } from 'date-fns';

import {
  BorderedContainer,
  SecondaryHeading,
  StepContainer,
  StepHeader,
} from '../Styled/StepContainer';
import './topic.scss';

export interface IFormState {
  topic: {
    [key: string]: any;
  };
  description: {
    [key: string]: any;
  };
  details: {
    [key: string]: any;
  };
}

const Review = (props: { state: IFormState }) => {
  const { topic, details, description } = props.state;
  return (
    <StepContainer>
      <StepHeader>
        <Typography variant="button" component="span">
          Review and Order
        </Typography>
        <Typography variant="subtitle2" component="span">
          Step 5 of 6
        </Typography>
      </StepHeader>

      <BorderedContainer>
        <SecondaryHeading>Topic</SecondaryHeading>

        <SecondaryHeading>Order Name</SecondaryHeading>

        <Typography variant="subtitle1">{topic.description}</Typography>

        <SecondaryHeading>Essay Type</SecondaryHeading>
        <Typography variant="subtitle1">{topic.type}</Typography>

        <SecondaryHeading>Service Required</SecondaryHeading>
        <Typography variant="subtitle1">Writing</Typography>
      </BorderedContainer>

      <BorderedContainer>
        <SecondaryHeading>Description</SecondaryHeading>

        <SecondaryHeading>Description</SecondaryHeading>

        <Typography variant="subtitle1">{description.description}</Typography>

        <SecondaryHeading>Uploaded Files</SecondaryHeading>
        {details.fileNames.map(file => (
          <Typography variant="subtitle1">{file}</Typography>
        ))}
      </BorderedContainer>

      <BorderedContainer>
        <SecondaryHeading>Details</SecondaryHeading>

        <Grid container={true} spacing={3}>
          <Grid item={true} xs={6}>
            <SecondaryHeading>Academic Level</SecondaryHeading>
            <Typography variant="subtitle1">{details.academicLevel}</Typography>
            <SecondaryHeading>Discipline</SecondaryHeading>
            <Typography variant="subtitle1">{details.discipline}</Typography>
            <SecondaryHeading>Style</SecondaryHeading>
            <Typography variant="subtitle1">{details.style}</Typography>
            <SecondaryHeading>Words</SecondaryHeading>
            <Typography variant="subtitle1">{details.words}</Typography>
          </Grid>
          <Grid item={true} xs={6}>
            <SecondaryHeading>Spacing</SecondaryHeading>
            <Typography variant="subtitle1">{details.spacing}</Typography>
            <SecondaryHeading>Bibliography</SecondaryHeading>
            <Typography variant="subtitle1">{details.biography ? 'Yes' : 'No'}</Typography>
            <SecondaryHeading>No of pages</SecondaryHeading>
            <Typography variant="subtitle1">{details.pages}</Typography>
            <SecondaryHeading>Deadline</SecondaryHeading>
            <Typography variant="subtitle1">
              {differenceInDays(new Date(details.deadlineDate), new Date())} days
            </Typography>
          </Grid>
        </Grid>
      </BorderedContainer>

      <BorderedContainer>
        <SecondaryHeading>Budget</SecondaryHeading>

        <SecondaryHeading>Fixed Price</SecondaryHeading>
        <SecondaryHeading>$50</SecondaryHeading>
      </BorderedContainer>
    </StepContainer>
  );
};

export default Review;
