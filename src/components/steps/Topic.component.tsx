import { FormControlLabel, Radio, TextField, Typography } from '@material-ui/core';
import {
  BorderedContainer,
  DescriptionFieldContainer,
  FormContent,
  InputWithLabel,
  SecondaryHeading,
  StepContainer,
  StepHeader,
} from '../Styled/StepContainer';

import './topic.scss';
import { IStepState } from './interfaces';

const Topic = ({ state }: IStepState) => {
  const [formState, setState] = state;
  const { topic } = formState;

  const handleOnChange = event => {
    setState({
      ...formState,
      topic: {
        ...formState.topic,
        [event.target.name]: event.target.value,
      },
    });
  };

  return (
    <StepContainer>
      <StepHeader>
        <Typography variant="button" component="span">
          Getting Started
        </Typography>
        <Typography variant="subtitle2" component="span">
          Step 1 of 6
        </Typography>
      </StepHeader>

      <FormContent>
        <DescriptionFieldContainer>
          <SecondaryHeading>Enter the name of your Order</SecondaryHeading>
          <TextField
            id="outlined-textarea"
            label="Description"
            rows={8}
            placeholder="Description"
            multiline={true}
            fullWidth={true}
            margin="normal"
            variant="outlined"
            name="description"
            value={topic.description || ''}
            onChange={handleOnChange}
          />
        </DescriptionFieldContainer>

        <Typography variant="subtitle1" component="span">
          Examples
        </Typography>
        <ul>
          <li>
            <Typography variant="subtitle1" component="span">
              Lorem Ipsum
            </Typography>
          </li>
          <li>
            <Typography variant="subtitle1" component="span">
              Gangster Lorem
            </Typography>
          </li>
          <li>
            <Typography variant="subtitle1" component="span">
              Foshizzle my sizzle
            </Typography>
          </li>
        </ul>

        <BorderedContainer>
          <InputWithLabel>
            <SecondaryHeading>Service Required</SecondaryHeading>

            <div className={'row'}>
              <FormControlLabel
                value={topic.service}
                control={
                  <Radio
                    checked={topic.service === 'Writing'}
                    onChange={handleOnChange}
                    value={'Writing'}
                    name="service"
                    color="primary"
                    inputProps={{ 'aria-label': 'service' }}
                  />
                }
                label="Writing"
                labelPlacement="end"
              />
              <FormControlLabel
                value={topic.service}
                control={
                  <Radio
                    checked={topic.service === 'Editing'}
                    onChange={handleOnChange}
                    value={'Editing'}
                    color="primary"
                    name="service"
                    inputProps={{ 'aria-label': 'service' }}
                  />
                }
                label="Editing"
                labelPlacement="end"
              />

              <FormControlLabel
                value={topic.service}
                control={
                  <Radio
                    checked={topic.service === 'Problem solving'}
                    onChange={handleOnChange}
                    value={'Problem solving'}
                    color="primary"
                    name="service"
                    inputProps={{ 'aria-label': 'service' }}
                  />
                }
                label="Problem solving"
                labelPlacement="end"
              />
            </div>
          </InputWithLabel>
        </BorderedContainer>
      </FormContent>
    </StepContainer>
  );
};

export default Topic;
