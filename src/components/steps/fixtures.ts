export const academicLevels = [
  {
    label: 'Bachelor',
    value: 'BSC',
  },
  {
    label: 'Certificate',
    value: 'Cert',
  },
  {
    label: 'Diploma',
    value: 'Dip',
  },
  {
    label: 'Masters',

    value: 'Msc',
  },
];
export const styles = [
  {
    label: 'Apa',
    value: 'apa',
  },
  {
    label: 'Pale',
    value: 'Pale',
  },
  {
    label: 'Huko',
    value: 'Huko',
  },
];

export const allPages = [
  {
    label: '1',
    value: '1',
  },
  {
    label: '2',
    value: '2',
  },
  {
    label: '3',
    value: '3',
  },
  {
    label: '4',

    value: '4',
  },
];
