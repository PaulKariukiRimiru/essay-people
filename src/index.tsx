import * as ReactDOM from "react-dom";

import "./index.scss";
import registerServiceWorker from "./registerServiceWorker";
import Routes from "./pages/Routes";
import React from "react";
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
