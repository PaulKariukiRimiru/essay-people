import { Outlet } from "react-router-dom";

import { Footer } from "../../components/Footer";

import "./App.scss";
import { Header } from "../../components/Header";



function App() {
  return (
    <>
      <Header />
      <Outlet />
      <Footer />
    </>
  );
}

export default App;
