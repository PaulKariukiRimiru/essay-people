import React, { useState } from 'react';

import {
  BottomNavigation,
  BottomNavigationAction,
  Typography,
  Fab,
  Snackbar,
} from '@material-ui/core';
import { useNavigate } from 'react-router-dom';

import { StyledCard } from '../../components/Styled';
import { Login } from '../../components/Login';
import { Register } from '../../components/Register';
import http from '../../utils/http';
import { MySnackbarContentWrapper } from '../../components/SnackBar';
import MyLoader from '../../components/Loader';

import './auth.scss';

export enum SnackBarVariants {
  Success = 'success',
  Warning = 'warning',
  Info = 'info',
  Error = 'error',
}

export const AuthPage = () => {
  const [value, setValue] = useState('login');
  const mainState = useState({
    username: '',
    email: '',
    password: '',
  });
  const [messageData, setMessageData] = useState({
    open: false,
    variant: SnackBarVariants.Success,
    message: '',
  });
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const [authState, setAuthState] = mainState;

  const handleOnchage = event => {
    setAuthState({
      ...authState,
      [event.target.name]: event.target.value,
    });
  };

  const handleBottomNavigationChange = (event, newValue) => {
    setValue(newValue);
  };

  const toggleLoader = () => {
    setLoading(!loading);
  };
  const handleSubmit = () => {
    toggleLoader();

    http
      .post(value === 'login' ? 'user/login' : 'users', JSON.stringify({ user: authState }))
      .then(resp => {
        setLoading(false);
        if (value === 'login') {
          localStorage.setItem('auth_token', resp.data.user.token);
          setMessageData({
            ...messageData,
            open: true,
            variant: SnackBarVariants.Success,
            message: 'login successful',
          });
        } else {
          setMessageData({
            ...messageData,
            open: true,
            variant: SnackBarVariants.Success,
            message: 'registration successful',
          });
        }
      })
      .catch(err => {
        setLoading(false);
        setMessageData({
          ...messageData,
          open: true,
          variant: SnackBarVariants.Error,
          message: err.response ? err.response.data.errors.error[0] : err.message,
        });
      });
  };

  const getComponentToRender = (
    selectedValue: string,
    state: [
      {
        username: string;
        email: string;
        password: string;
      },
      React.Dispatch<any>,
    ],
    handleChange: (event: any) => void,
  ) => {
    switch (selectedValue) {
      case 'login':
        return <Login state={state} handleChange={handleChange} />;
      case 'register':
        return <Register state={state} handleChange={handleChange} />;
      default:
        return <div>Wrong choice :-o</div>;
    }
  };

  const handleSnackBarClose = () => {
    if (value === 'login') {
      if (messageData.variant === SnackBarVariants.Success) {
        navigate('/');

        // TODO: Find a cleaner approach
        window.location.reload();
      }
    } else {
      if (messageData.variant === SnackBarVariants.Success) {
        setValue('login');
      }
    }

    setMessageData({
      ...messageData,
      open: false,
    });
  };

  return (
    <div className="auth">
      <StyledCard
        width="25vw"
        height="60vh"
        radius="28px"
        margin="56px"
        color="#707070"
        backgroundtype="color"
        background="#fff"
        boxShadow="0 3px 28px 0 rgba(0, 0, 0, 0.16)"
      >
        <div className="auth-container">
          <Typography variant="h5" gutterBottom={true} className="auth-header">
            {value === 'login' ? 'Sign in' : 'Sign up'}
          </Typography>
          {getComponentToRender(value, mainState, handleOnchage)}
          <Fab variant="extended" aria-label="like" className="auth-submit" onClick={handleSubmit}>
            {value === 'login' ? 'Sign in' : 'Sign up'}
          </Fab>
          <BottomNavigation
            value={value}
            onChange={handleBottomNavigationChange}
            showLabels={true}
            className="auth-bottom"
          >
            <BottomNavigationAction label="Sign in" value="login" />
            <BottomNavigationAction label="Sign up" value="register" />
          </BottomNavigation>
        </div>
      </StyledCard>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={messageData.open}
        autoHideDuration={2000}
        onClose={handleSnackBarClose}
      >
        <MySnackbarContentWrapper
          onClose={handleSnackBarClose}
          variant={messageData.variant}
          message={messageData.message}
        />
      </Snackbar>
      <MyLoader open={loading} />
    </div>
  );
};
