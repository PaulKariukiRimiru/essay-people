export const headliners = ['TOTAL', 'COMPLETED', 'PENDING', 'REJECTED'].map(title => ({
  title: `${title} ORDERS`,
  total: 100,
}));

export const sampleOrders = [
  {
    id: 'ORDER_ID',
    title: 'Order title',
    client: 'John Doe',
    status: 'Accepted',
  },
  {
    id: 'ORDER_ID',
    title: 'Order title',
    client: 'Jane Doe',
    status: 'Rejected',
  },
  {
    id: 'ORDER_ID',
    title: 'Order title',
    client: 'John Doe',
    status: 'Pending',
  },
];
