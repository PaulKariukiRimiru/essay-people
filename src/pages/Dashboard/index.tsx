import { useState, useEffect } from 'react';

import { Snackbar } from '@material-ui/core';

import { StyledDiv } from '../../components/Styled/Div';
import { MySnackbarContentWrapper } from '../../components/SnackBar';
import MyLoader from '../../components/Loader';
import http from '../../utils/http';

import { SnackBarVariants } from '../Auth';
import { IOrderDetails, OrderStatus, IOrdersReport } from './interfaces';
import './dashboard.scss';

const Dashboard = () => {
  const [loading, setloading] = useState(false);
  const [messageData, setmessageData] = useState({
    open: false,
    variant: SnackBarVariants.Success,
    message: '',
  });
  const [orders, setorders] = useState([] as IOrderDetails[]);
  const [ordersReport, setordersReport] = useState({
    total: 0,
    completed: 0,
    pending: 0,
    rejected: 0,
  } as IOrdersReport);

  useEffect(() => {
    setloading(true);

    const fetchOrders = () => {
      http
        .get('orders/all')
        .then(resp => {
          const { results, count } = resp.data.orders;
          setloading(false);
          setorders(results);
          setordersReport({
            total: count,
            completed: results.filter(order => order.order_status === 2).length,
            pending: results.filter(order => order.order_status === 1 || order.order_status === 0)
              .length,
            rejected: results.filter(order => order.order_status === 3).length,
          });
          setmessageData({
            open: true,
            variant: SnackBarVariants.Success,
            message: 'orders retrieval success',
          });
        })
        .catch(err => {
          setloading(false);
          setmessageData({
            open: true,
            variant: SnackBarVariants.Success,
            message: err.message,
          });
        });
    };

    fetchOrders();
  }, []);

  const getOrderColor = (status: number) => {
    switch (status) {
      case 0:
        return '#D9C632';
      case 1:
        return '#1CACF4';
      case 2:
        return '#31CB24';
      case 3:
        return '#F81B7A';
      default:
        return '#D9C632';
    }
  };

  const convertStatus = (orderStatus: number) => {
    switch (orderStatus) {
      case 0:
        return OrderStatus.UnAssigned;
      case 1:
        return OrderStatus.AssignedAndPending;
      case 2:
        return OrderStatus.Completed;
      case 3:
        return OrderStatus.RejectedAndRefunded;
      default:
        return OrderStatus.Unknown;
    }
  };

  const handleSnackBarClose = () => {
    setmessageData({
      ...messageData,
      open: false,
    });
  };

  return (
    <div className="dashboard">
      <div className="dashboard-header">
        {Object.entries(ordersReport).map(([title, total]) => (
          <div className="dashboard-header__section">
            <div className="dashboard-header__section-title">{title} orders</div>
            <div className="dashboard-header__section-total">{total}</div>
          </div>
        ))}
      </div>
      <div className="dashboard-container">
        <div className="dashboard-container__orders">
          {orders.map(({ topic, client, order_status, id }) => (
            <div className="dashboard-container__orders-container">
              <StyledDiv
                width="3px"
                height="66px"
                backgroundColor={getOrderColor(order_status)}
                color="#31CB24"
              />
              <div className="dashboard-container__orders-order">
                <div className="dashboard-container__orders-order__id">{id}</div>
                <div className="dashboard-container__orders-order__container">
                  <div className="dashboard-container__orders-order__container-title">
                    <span className="description">Title</span>
                    {topic}
                  </div>
                  <div className="dashboard-container__orders-order__container-client">
                    <span className="description">Client</span>
                    {client.username}
                  </div>
                  <div className="dashboard-container__orders-order__container-status">
                    <span className="description">Status</span>
                    {convertStatus(order_status)}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={messageData.open}
        autoHideDuration={2000}
        onClose={handleSnackBarClose}
      >
        <MySnackbarContentWrapper
          variant={messageData.variant}
          message={messageData.message}
          onClose={handleSnackBarClose}
        />
      </Snackbar>
      <MyLoader open={loading} />
    </div>
  );
};

export default Dashboard;
