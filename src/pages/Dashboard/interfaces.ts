export interface IOrderDetails {
  id: number;
  topic: string;
  order_status: number;
  client: { username: string };
}

export interface IOrdersReport {
  total: number;
  completed: number;
  pending: number;
  rejected: number;
}

export enum OrderStatus {
  UnAssigned = 'Un Assigned',
  AssignedAndPending = 'Assigned And Pending',
  Completed = 'Completed',
  RejectedAndRefunded = 'Rejected And Refunded',
  Unknown = 'Unknown',
}
