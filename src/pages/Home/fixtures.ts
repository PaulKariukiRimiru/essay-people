export const categories = [
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569270482/iconmonstr-school-19-240.png',
    name: 'High schools',
  },
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569270482/iconmonstr-education-1-240.png',
    name: 'UnderGraduates',
  },
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569270482/iconmonstr-certificate-15-240.png',
    name: 'Masters',
  },
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569270482/iconmonstr-certificate-8-240.png',
    name: `Phd's`,
  },
];

export const orderProcess = [
  {
    description: 'Fill in a from describing the kind of Paper you want',
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569307542/iconmonstr-file-23-240.png',
    title: 'Fill Out Order',
  },
  {
    description: 'Fill in a from describing the kind of Paper you want',
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569307542/iconmonstr-calculator-5-240.png',
    title: 'Calculate Price',
  },
  {
    description: 'Fill in a from describing the kind of Paper you want',
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569307542/iconmonstr-debit-6-240.png',
    title: 'Payment Details',
  },
  {
    description: 'Fill in a from describing the kind of Paper you want',
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569307542/iconmonstr-pen-4-240.png',
    title: 'Get Paper Done',
  },
];

export const pricingRanges = [
  {
    deliveryTime: '24',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    level: 'High School',
    price: '18',
  },
  {
    deliveryTime: '24',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    level: 'UnderGraduate',
    price: '22',
  },
  {
    deliveryTime: '24',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    level: 'Masters',
    price: '28',
  },
  {
    deliveryTime: '24',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    level: 'Phd',
    price: '39',
  },
];

export const testimonials = [
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569552429/Screen_Shot_2019-01-20_at_23.50.46.png',
    testimony:
      'Essay People does not compromise on Quality And aims to deliver their papers on time because the quick brown fox jumped over the lazy dog',
    title: 'Chill Master',
    user: 'Kalela',
  },
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569552429/Screen_Shot_2019-01-20_at_23.49.08.png',
    testimony:
      'Essay People does not compromise on Quality And aims to deliver their papers on time because the quick brown fox jumped over the lazy dog',
    title: 'Masters',
    user: 'Rodgers',
  },
  {
    image:
      'https://res.cloudinary.com/mikekrantz/image/upload/v1569552429/Screen_Shot_2019-01-20_at_23.48.25.png',
    testimony:
      'Essay People does not compromise on Quality And aims to deliver their papers on time because the quick brown fox jumped over the lazy dog',
    title: 'Phd',
    user: 'Michael Mukalo',
  },
];

export const payments = [
  'https://res.cloudinary.com/mikekrantz/image/upload/v1569555207/iconmonstr-payment-3-240.png',
  'https://res.cloudinary.com/mikekrantz/image/upload/v1569555241/iconmonstr-payment-10-240.png',
  'https://res.cloudinary.com/mikekrantz/image/upload/v1569555241/iconmonstr-payment-28-240.png',
  'https://res.cloudinary.com/mikekrantz/image/upload/v1569555241/iconmonstr-payment-25-240.png',
  'https://res.cloudinary.com/mikekrantz/image/upload/v1569555241/iconmonstr-payment-27-240.png',
];
