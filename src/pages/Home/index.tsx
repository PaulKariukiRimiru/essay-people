import { Slide } from "react-slideshow-image";

import { StyledCard } from "../../components/Styled/Card";
import { StyledDiv } from "../../components/Styled/Div";

import {
  categories,
  orderProcess,
  payments,
  pricingRanges,
  testimonials,
} from "./fixtures";

import "./home.scss";
import 'react-slideshow-image/dist/styles.css'

export const Home = () => (
  <div className="home">
    <StyledCard
      width="95vw"
      height="450px"
      radius="64px"
      backgroundtype="image"
      background="https://res.cloudinary.com/mikekrantz/image/upload/v1569306662/Screen_Shot_2019-02-03_at_09.29.10.png"
      color="#fff"
    >
      <div className="home-headline">
        What if you could get Competent, well thought out, Authentic Academic
        Papers?
      </div>
      <div className="home-subtitle">WE CURATE ACADEMIC PAPERS FOR YOU</div>
    </StyledCard>
    <div className="home-categories">
      {categories.map(({ name, image }) => (
        <StyledCard
          width="200px"
          height="200px"
          radius="28px"
          margin="36px"
          color="#707070"
          backgroundtype="color"
          background="#fff"
          boxShadow="0 3px 28px 0 rgba(0, 0, 0, 0.16)"
        >
          <div className="home-categories__title">{name}</div>
          <img className="home-categories__icon" src={image} alt="" />
        </StyledCard>
      ))}
    </div>
    <div className="home-products">
      <div className="home-products__order">
        <div className="home-products-title">Make an Order</div>
        <div className="home-products__container">
          <img
            className="home-products__container-image"
            src="https://res.cloudinary.com/mikekrantz/image/upload/v1569272850/Screen_Shot_2019-01-20_at_23.55.35.png"
          />
          <div className="home-products__container-button">
            <div className="home-products__container-button-text">Order</div>
            <img
              className="home-products__container-button-img"
              src="https://res.cloudinary.com/mikekrantz/image/upload/v1569273772/Forward_arrow.png"
            />
          </div>
        </div>
      </div>
      <div className="home-products__divider" />
      <div className="home-products__sample">
        <div className="home-products-title">View Samples</div>
        <div className="home-products__container">
          <img
            className="home-products__container-image"
            src="https://res.cloudinary.com/mikekrantz/image/upload/v1569272850/Screen_Shot_2019-01-20_at_23.55.46.png"
          />
          <div className="home-products__container-button">
            <div className="home-products__container-button-text">Preview</div>
            <img
              className="home-products__container-button-img"
              src="https://res.cloudinary.com/mikekrantz/image/upload/v1569273772/Forward_arrow.png"
            />
          </div>
        </div>
      </div>
    </div>
    <div className="home-process">
      <div className="home-process-title">How it works</div>
      <div className="home-process__container">
        {orderProcess.map(({ image, description, title }) => (
          <div className="home-process__container-item">
            <img
              className="home-process__container-item-image"
              src={image}
              alt=""
            />
            <div className="home-process__container-item-title">{title}</div>
            <div className="home-process__container-item-divider" />
            <div className="home-process__container-item-description">
              {description}
            </div>
          </div>
        ))}
      </div>
    </div>
    <div className="home-pricing">
      <div className="home-pricing-title">Pricing</div>
      <div className="home-pricing__container">
        {pricingRanges.map(({ level, price, description, deliveryTime }) => (
          <StyledCard
            radius="28px"
            margin="36px"
            color="#fff"
            backgroundtype="color"
            background="#445cc7"
            className="home-pricing__container-item"
          >
            <img
              className="home-pricing__container-item-image"
              src="https://res.cloudinary.com/mikekrantz/image/upload/v1569310242/icon.png"
            />
            <div className="home-pricing__container-item-level">{level}</div>
            <div className="home-pricing__container-item-price">
              <sup className="superscript">$</sup>
              {price}
            </div>
            <div className="home-pricing__container-item-description">
              per paper
            </div>
            <div className="home-pricing__container-item-description">
              {description}
            </div>
            <div className="home-pricing__container-item-divider" />
            <div className="home-pricing__container-item-description">
              Delivered in {deliveryTime} hrs
            </div>
            <div className="home-pricing__container-item-divider" />
            <div className="home-pricing__container-item-button">
              TRY IT OUT
            </div>
          </StyledCard>
        ))}
      </div>
      <div className="home-pricing-link">
        Checkout our complete{" "}
        <a className="home-pricing-link-link" href="">
          pricing schedule
        </a>
      </div>
    </div>
    <div className="home-testimonials">
      <div className="home-testimonials-title">
        Why do people love essay people
      </div>
      <Slide
        duration={5000}
        transitionDuration={500}
        infinite={true}
        indicators={true}
        arrows={true}
      >
        {testimonials.map(({ image, testimony, user, title }) => (
          <div className="home-testimonials__item">
            <img className="home-testimonials__item-image" src={image} alt="" />
            <div className="home-testimonials__item-testimony">{testimony}</div>
            <div className="home-testimonials__item-user">
              {user} ~{" "}
              <span className="home-testimonials__item-user-title">
                {title}
              </span>
            </div>
          </div>
        ))}
      </Slide>
    </div>
    <div className="home-payments">
      <div className="home-payments-title">Verified payment methods</div>
      <div className="home-payments__container">
        {payments.map((payment) => (
          <img
            className="home-payments__container-image"
            src={payment}
            alt=""
          />
        ))}
      </div>
    </div>
    <StyledDiv
      width="100vw"
      height="337px"
      backgroundColor="#445cc7"
      backgroundImage="https://res.cloudinary.com/mikekrantz/image/upload/v1569306662/Screen_Shot_2019-02-03_at_09.29.10.png"
      color="#fff"
      className="home-contacts"
    >
      <div className="home-contacts-title">Ready to work with us?</div>
      <div className="home-contacts-button">Order now</div>
    </StyledDiv>
  </div>
);
