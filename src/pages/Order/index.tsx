import Budget from '../../components/steps/Budget';
import React, { useState, useRef } from 'react';

import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import Typography from '@material-ui/core/Typography';

import Review from '../../components/steps/Review';
import Description from '../../components/steps/Description';
import Details from '../../components/steps/Details.component';
import Topic from '../../components/steps/Topic.component';
import { getFirestoreInstance } from '../../utils/firebase';
import http from '../../utils/http';

import './order.scss';
import { SnackBarVariants } from '../Auth';
import { Snackbar } from '@material-ui/core';
import { MySnackbarContentWrapper } from '../../components/SnackBar';
import MyLoader from '../../components/Loader';

function getSteps() {
  return ['Topic', 'Description', 'Details', 'Budget', 'Review', 'Payment'];
}

export default function Orders() {
  const [activeStep, setActiveStep] = useState(0);
  const [skipped, setSkipped] = useState(new Set<number>());
  const orderFormState = useState<{ [key: string]: Partial<{ [key: string]: any }>}>({
    topic: {},
    description: {},
    details: {},
  });
  const formStates = useState<{ [key: string]: any }>({});
  const [messageData, setMessageData] = useState({
    open: false,
    variant: SnackBarVariants.Success,
    message: '',
  });
  const [loading, setLoading] = useState(false);

  const steps = getSteps();

  const inputFile = useRef(null);

  const isStepOptional = (step: number) => {
    return false;
  };

  const isStepSkipped = (step: number) => {
    return skipped.has(step);
  };

  const firebaseInstance = getFirestoreInstance();

  const handleSnackBarClose = () => {
    setMessageData({
      ...messageData,
      open: false,
    });
  };

  const handleNext = async () => {
    const [order, setOrderState] = orderFormState;
    if (activeStep === 1) {
      setLoading(true);
      const current = (inputFile.current as unknown) as any;
      const files = current.files;

      let uploadedFiles: string[] = [];
      let failedPaths: any[] = [];

      // tslint:disable-next-line:prefer-for-of
      for (let index = 0; index < files.length; index++) {
        const path = await firebaseInstance
          .ref(`essay/${Date.now()}-${files[index].name}`)
          .put(files[index])
          .then(snapshot => snapshot.metadata.fullPath)
          .catch(error => console.error(error));

        if (!path) {
          failedPaths.push(files[index])
        } else {
          uploadedFiles.push(path)
        }
      }

      setLoading(false);
      setOrderState({
        ...order,
        details: {
          ...order.details,
          fileNames: uploadedFiles,
        },
      });
    }

    if (activeStep === steps.length - 1) {
      setLoading(true);
      http
        .post('orders/', {
          order: {
            academic_level: order.details.academicLevel,
            order_type: order.topic.service,
            deadline_date: new Date(order.details.deadlineDate).toLocaleDateString(),
            deadline_time: new Date(order.details.deadlineTime).toLocaleTimeString('en-US'),
            pages: order.details.pages,
            words: order.details.word,
            topic: order.topic.description,
            paper_instructions: order.description.description,
            discipline: order.details.discipline,
            paper_style: order.details.style,
            // TODO: Fix when ammount page is complete
            amount: 50,
            amount_paid: 25,
            file_name: order.details.fileNames.map(path => ({ file_name: path })),
          },
        })
        .then(resp => {
          setLoading(false);
          setMessageData({
            ...messageData,
            open: true,
            variant: SnackBarVariants.Success,
            message: 'Order saved successfully',
          });
        })
        .catch(err => {
          setLoading(false);
          setMessageData({
            ...messageData,
            open: true,
            variant: SnackBarVariants.Error,
            message: err.message,
          });
        });
    }

    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep(prevActiveStep => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep(prevActiveStep => prevActiveStep + 1);
    setSkipped(prevSkipped => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const renderOrderFormSection = (section: number, state: any[]) => {
    switch (section) {
      case 0:
        return <Topic state={orderFormState} />;
      case 1:
        return <Description state={orderFormState} inputFile={inputFile} />;
      case 2:
        return <Details state={orderFormState} />;
      case 3:
        return <Budget />;
      case 4:
        return <Review state={orderFormState[0] as any} />;
      default:
        return <div>coming soon</div>;
    }
  };

  return (
    <div className="order">
      <Stepper activeStep={activeStep} className="order-stepper">
        {steps.map((label, index) => {
          const stepProps: {
            completed?: boolean;
          } = {};
          const labelProps: {
            optional?: React.ReactNode;
          } = {};
          if (isStepOptional(index)) {
            labelProps.optional = <Typography variant="caption">Optional</Typography>;
          }
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>

      <div className="order__container">{renderOrderFormSection(activeStep, formStates)}</div>

      <div className="order__buttons">
        {activeStep === steps.length ? (
          <div>
            <Typography className="instructions">
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className="button">
              Reset
            </Button>
          </div>
        ) : (
          <div>
            <div className="navigation__buttons">
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className="button"
                variant="contained"
              >
                Back
              </Button>
              {isStepOptional(activeStep) && (
                <Button variant="contained" color="primary" onClick={handleSkip} className="button">
                  Skip
                </Button>
              )}
              <Button variant="contained" color="primary" onClick={handleNext} className="button">
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </div>
          </div>
        )}
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={messageData.open}
        autoHideDuration={2000}
        onClose={handleSnackBarClose}
      >
        <MySnackbarContentWrapper
          onClose={handleSnackBarClose}
          variant={messageData.variant}
          message={messageData.message}
        />
      </Snackbar>
      <MyLoader open={loading} />
    </div>
  );
}
