import { useNavigate, useRoutes } from "react-router-dom";

import App from "./App";
import { Home } from "./Home";
import { AuthPage } from "./Auth";
import Dashboard from "./Dashboard";
import Orders from "./Order";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const navigate = useNavigate();
  if (!localStorage.getItem("auth_token")) {
    navigate(`/auth`);
  }
  return <Component {...rest}/>;
};

export default function Router() {
  const routes = useRoutes([
    {
      path: "/",
      element: <App />,
      children: [
        { index: true, element: <Home /> },
        { path: "auth", element: <AuthPage /> },
        { path: "order", element: <Orders /> },
        { path: "dashboard", element: <PrivateRoute component={Dashboard} />,  },
      ],
    },
  ]);
  return routes
}
