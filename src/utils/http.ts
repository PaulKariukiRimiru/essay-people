import axios from 'axios';

const defaultHeaders = {
  'Content-Type': 'application/json',
};

export default axios.create({
  baseURL: 'http://f-bend.herokuapp.com/api/',
  // timeout: 1000,
  headers: localStorage.getItem('auth_token')
    ? {
        ...defaultHeaders,
        Authorization: `Token ${localStorage.getItem('auth_token')}`,
      }
    : defaultHeaders,
});
